# Latest release version:

Version 0.2.2: [Download](https://drive.google.com/file/d/13J6jBELm60AK1SqpIJeVdA0Zli-ghkLw/view?usp=sharing)

# Version history

## 0.2.2

 * Add scale to object copy feature

## 0.2.1

 * Add object copy & transform function to object mode panel

## 0.2.0

 * Add collection importing to asset loader
 * UI updates
 * Internal propertygroup update
 * Installation from .zip file


## 0.1.4

 * Add UV pack tool
    - Trying to pack UV faces near the actual texture for improving the material mapping

## 0.1.3

  Fixes:

  * Fixed EnumProperty assignment for 2.93 API

## 0.1.2

  Fixes:

  * Fixed asset loader duplicate linking if object is already in scene in another collection

## 0.1.1

  Features:

  * UV rotating tool (in edit mode)

## 0.1.0

  Features:

  * Asset loader
  * RBR Split
  * Create Proxy
  * Transform objects

  Updates:

  * Asset loader 'use root path' works now properly
  * Added options to Transform objects
  * Other UI refining
