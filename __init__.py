bl_info = {
    "name": "Jorski's RBR tools",
    "author": "Joona Karimaa",
    "version": (0, 2, 2),
    "blender": (2, 93, 4),
    "category": "RBR",
    "location": "N Menu/Scene properties",
    "description": "Jorski's toolbox for RBR track making",
    "warning": "",
    "doc_url": "",
    "tracker_url": "",
    }

"""
Including:

RBR Splitter
Assets linking

Vertex Color shadow baking (obsolete)
.X exporter for wallaby (deprecated)

"""

import bpy

from bpy.props import IntProperty, EnumProperty, StringProperty, BoolProperty

# When bpy is already in local, we know this is not the initial import...
if "bpy" in locals():
    # ...so we need to reload our submodule(s) using importlib
    import importlib
    if "ui" in locals():
        importlib.reload(ui)
    if "functions" in locals():
        importlib.reload(functions)

from . import ui

# from bpy.types import PropertyGroup
# from bpy.utils import register_class, unregister_class

class RBRProperties(bpy.types.PropertyGroup):

    rbr_split_range : IntProperty(
        name = "",
        description = "split range",
        default = 100,
        soft_min = 25,
        soft_max = 250)

    rbr_proxy_range : IntProperty(
        name = "",
        description = "proxy count",
        default = 1,
        soft_min = 1,
        soft_max = 25)

    rbr_category : EnumProperty(
        items = [('item', 'Item', 'description')],
        name = "")

    rbr_exclude : StringProperty(
        name = "Exclude folders",
        default = "source_files, WIP")

    rbr_project_path : StringProperty(
        name = "Project path",
        default = "C:\\Users\\joona\\Documents\\Dropbox\\HarjuModels",
        subtype = "DIR_PATH")

    rbr_connected : BoolProperty(
        name = "",
        default = False)

    rbr_make_proxy : BoolProperty(
        name = "Make proxy",
        default = True)

    rbr_link : BoolProperty(
        name = "Link",
        default = True)

    rbr_use_root : BoolProperty(
        name = "Use root path",
        default = False)

    rbr_transform_rotation : BoolProperty(
        name = "Rotation",
        description = 'Transform rotation',
        default = True)

    rbr_transform_location : BoolProperty(
        name = 'Location',
        description = 'Transform location',
        default = True)

    rbr_transform_scale : BoolProperty(
        name = 'Scale',
        description = 'Transform scale',
        default = True)

    rbr_use_collection_import : BoolProperty(
        name = "Use collection import",
        default = True)


classes = (
    RBRProperties,
    ui.VIEW3D_PT_rbr_tools,
    ui.VIEW3D_PT_rbr_tools_editmode,
    ui.VIEW3D_PT_locrot_button,
    ui.VIEW3D_PT_copy_and_locrot_button,
    ui.VIEW3D_PT_split_button,
    ui.VIEW3D_PT_uvrotate_button,
    ui.VIEW3D_PT_uvmanipulate_button,
    ui.VIEW3D_PT_proxy_button,
    ui.VIEW3D_PT_connect_button,
    ui.VIEW3D_PT_import_button,
    ui.VIEW3D_PT_import_collection_button,
    ui.PANEL_PT_connect_menu,
    ui.WMFileSelector
)

def register():
    for cls in classes:
        bpy.utils.register_class(cls)
    bpy.types.Scene.jorskis_rbr_properties = bpy.props.PointerProperty(type=RBRProperties)

def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)
    del bpy.types.Scene.jorskis_rbr_properties
