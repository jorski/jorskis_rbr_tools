import bpy
from bpy.props import IntProperty, EnumProperty, StringProperty, BoolProperty

blend_dic = {}
collections_dic = {}

def copy_locrot(self, context):
    props = context.scene.jorskis_rbr_properties

    object_1 = bpy.context.object
    object_list = []

    if len(bpy.context.selected_objects) > 1:
        for object in bpy.context.selected_objects:
            if object != object_1:
                object_list.append(object)

        for object in object_list:
            if props.rbr_transform_location:
                object.location = object_1.location

            if props.rbr_transform_rotation:
                object.rotation_euler = object_1.rotation_euler

            if props.rbr_transform_scale:
                object.scale = object_1.scale

        return {'FINISHED'}

    self.report({'ERROR'}, 'No other objects selected')
    return {'CANCELLED'}

def copy_and_locrot(self, context):

    if len(bpy.context.selected_objects) > 1:
        ob_from = bpy.context.active_object
        obs_to = bpy.context.selected_objects

        # Remove active from selected
        obs_to.remove(ob_from)

        #Copy ob from to obs to locations & rotations
        for ob in obs_to:

            ob_from_copy = ob_from.copy()
            ob_from_copy.location = ob.location
            ob_from_copy.rotation_euler = ob.rotation_euler
            ob_from_copy.scale = ob.scale

            bpy.context.collection.objects.link(ob_from_copy)

        return {'FINISHED'}

    else:
        self.report({'ERROR'}, 'Not enough objects selected')
        return {'CANCELLED'}

def rbr_split(self, context):
    import bmesh

    rbr_props = context.scene.jorskis_rbr_properties
    prop_range = rbr_props.rbr_split_range

    object_size_x = context.active_object.dimensions.x / prop_range
    object_size_y = context.active_object.dimensions.y / prop_range

    if object_size_x > object_size_y:
        object_size = object_size_x
    else:
        object_size = object_size_y

    def cutPlane(self, x_direction, y_direction):
        for distance in range(int(object_size)):
            x_start = distance * x_direction
            x_end = prop_range + distance * x_direction

            for distance in range(int(object_size)):
                y_start = distance * y_direction
                y_end = prop_range + distance * y_direction

                selected_faces = []

                for face in bm.faces:
                    face_pos_x = face.calc_center_median()[0]
                    face_pos_y = face.calc_center_median()[1]

                    if face_pos_x >= x_start and face_pos_x < x_end:
                        if face_pos_y >= y_start and face_pos_y < y_end:
                            face.select = True
                            selected_faces.append(face)
                    else:
                        face.select = False



                if len(selected_faces) > 0:
                    bpy.ops.mesh.separate(type='SELECTED')

    bpy.ops.view3d.snap_cursor_to_active()
    bpy.ops.object.origin_set(type='ORIGIN_GEOMETRY')

    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.mesh.select_all(action='DESELECT')

    me = context.object.data
    bm = bmesh.from_edit_mesh(me)

    cutPlane(self, prop_range, prop_range)
    cutPlane(self, prop_range, -prop_range)
    cutPlane(self, -prop_range, prop_range)
    cutPlane(self, -prop_range, -prop_range)



    bmesh.update_edit_mesh(me)
    bm.free()
    bpy.ops.object.mode_set(mode='OBJECT')
    bpy.ops.object.origin_set(type='ORIGIN_CURSOR')

    return {'FINISHED'}

def uv_rotate():
    import bmesh
    import random

    from math import cos, sin, radians

    def make_rotation_transformation(angle, origin=(0, 0)):
        cos_theta, sin_theta = cos(angle), sin(angle)
        x0, y0 = origin
        def xform(point):
            x, y = point[0] - x0, point[1] - y0
            return (x * cos_theta - y * sin_theta + x0,
                    x * sin_theta + y * cos_theta + y0)
        return xform

    def calculate_centroid(vertices):
        x = 0
        y = 0
        for vert in vertices:
            x += vert.x
            y += vert.y

        centroid = (x / len(vertices), y / len(vertices))
        return centroid

    ob = bpy.context.active_object

    bpy.ops.mesh.split()
    bpy.ops.mesh.edge_split(type='EDGE')

    obj = bpy.context.edit_object
    me = obj.data
    bm = bmesh.from_edit_mesh(me)

    bm.verts.ensure_lookup_table()
    bm.faces.ensure_lookup_table()

    selected_faces = {}

    for id, face in  enumerate(bm.faces):
        if face.select == True:
            selected_faces[id] = True
        else:
            selected_faces[id] = False

    uv_layer = bm.loops.layers.uv[ob.data.uv_layers.active_index]

    nFaces = len(bm.faces)

    for fi in range(nFaces):
        if selected_faces[fi]:

            vertices = []

            for ver in bm.faces[fi].loops:
                vertices.append(ver[uv_layer].uv)

            rad = random.random() * 3.142
            anchor = calculate_centroid(vertices)

            rot = make_rotation_transformation(rad, anchor)

            for ver in bm.faces[fi].loops:

                ver[uv_layer].uv = rot(ver[uv_layer].uv)

    bpy.ops.mesh.select_all(action='SELECT')
    bpy.ops.mesh.remove_doubles(threshold=0.0001, use_unselected=False)
    bpy.ops.mesh.select_all(action='DESELECT')

def uv_manipulation():
    import bmesh
    from mathutils import Vector
    from math import floor

    def calculate_centroid(vertices):
        x = 0
        y = 0
        for vert in vertices:
            x += vert.x
            y += vert.y

        centroid = Vector((x / len(vertices), y / len(vertices)))
        return centroid

    def check_if_outside_of_area(vector):

        for co in vector:
            if co < 0:
                return True
            if co > 1:
                return True

        return False

    def make_transformation(origin, anchor):

        vert_x = origin[0]
        vert_y = origin[1]

        anchor_x = anchor[0]
        anchor_y = anchor[1]

        if anchor_x < 0:
            vert_x = vert_x - floor(anchor_x)

        if anchor_x > 1:
            vert_x = vert_x - floor(anchor_x)

        if anchor_y < 0:
            vert_y = vert_y - floor(anchor_y)

        if anchor_y > 1:
            vert_y = vert_y - floor(anchor_y)

        return Vector((vert_x, vert_y))

    ob = bpy.context.active_object

    bpy.ops.mesh.split()
    bpy.ops.mesh.edge_split(type='EDGE')

    obj = bpy.context.edit_object
    me = obj.data
    bm = bmesh.from_edit_mesh(me)

    bm.verts.ensure_lookup_table()
    bm.faces.ensure_lookup_table()

    selected_faces = {}

    for id, face in  enumerate(bm.faces):
        if face.select == True:
            selected_faces[id] = True
        else:
            selected_faces[id] = False

    uv_layer = bm.loops.layers.uv[ob.data.uv_layers.active_index]

    nFaces = len(bm.faces)

    for fi in range(nFaces):
        if selected_faces[fi]:

            vertices = []

            for ver in bm.faces[fi].loops:
                vertices.append(ver[uv_layer].uv)

            anchor = calculate_centroid(vertices)
            outside = check_if_outside_of_area(anchor)

            if outside:
                print(anchor)

                for ver in bm.faces[fi].loops:
                   ver[uv_layer].uv = make_transformation(ver[uv_layer].uv, anchor)

    bpy.ops.mesh.select_all(action='SELECT')
    bpy.ops.mesh.remove_doubles(threshold=0.0001, use_unselected=False)
    bpy.ops.mesh.select_all(action='DESELECT')

def connect(self, context):
    import os

    scene = context.scene
    rbr_props = scene.jorskis_rbr_properties

    path = None
    if rbr_props.rbr_use_root:
        # abs_path = os. bpy.data.filepath
        path = os.path.dirname(os.path.realpath(bpy.data.filepath))
        rbr_props.rbr_project_path = path
        # split = abs_path.rsplit('\\', 1)
        # path = split[0] + '\\'
    else:
        path = rbr_props.rbr_project_path

    if os.path.isdir(path):
        list = os.listdir(path)

        for folder in list:
            file_list = []
            if not '.' in folder and folder not in rbr_props.rbr_exclude:
                files = os.listdir('{0}\\{1}'.format(path, folder))
                for file in files:
                    if '.blend' in file:
                        if not '.' in file and not '.blend1' in file:
                            subfolder = os.listdir('{0}\\{1}\\{2}\\'.format(path, folder, file))
                            for subfile in subfolder:
                                if '.blend' in subfile and not '.blend1' in subfile:
                                    atrr_list = ['{}/{}'.format(file, subfile)]
                                    filepath = '{0}\\{1}\\{2}\\{3}'.format(path, folder, file, subfile)

                                    if rbr_props.rbr_use_collection_import:
                                        atrr_list.append(get_collections_in_blend(filepath))
                                    else:
                                        atrr_list.append(check_existence(filepath))

                                    file_list.append(atrr_list)
                        else:

                            atrr_list = [file]
                            filepath = '{0}\\{1}\\{2}'.format(path, folder, file)

                            if rbr_props.rbr_use_collection_import:
                                atrr_list.append(get_collections_in_blend(filepath))
                            else:
                                atrr_list.append(check_existence(filepath))

                            file_list.append(atrr_list)

                blend_dic[folder] = file_list

        prop = []
        for i, ob in enumerate(blend_dic):
            prop.insert(i,(ob, ob.title(), 'Description'))

        if prop:
            group = bpy.types.Scene.jorskis_rbr_properties
            group.keywords['type'].rbr_category = EnumProperty(
                                         items=prop, name=''
                                         )
    rbr_props.rbr_connected = True
    return {'FINISHED'}

def check_existence(filepath):

    ob_bool_list = []
    object_list = []
    for object in bpy.data.objects:
        object_list.append(object.name)
    with bpy.data.libraries.load(filepath, link=True, relative=True) as (data_from, data_to):
        data_to.objects = [name for name in data_from.objects if name in object_list]
        for ob in data_from.objects:
            if ob in object_list:
                ob_bool_list.append(True)
            else:
                ob_bool_list.append(False)

    sum = len(ob_bool_list)
    trues = 0
    falses = 0
    for bool in ob_bool_list:
        if bool:
            trues += 1
        else:
            falses +=1
    amount_of_true = trues / sum * 100

    return amount_of_true

def get_collections_in_blend(filepath):
    collections = []

    with bpy.data.libraries.load(filepath, link=True, relative=True) as (data_from, data_to):
        for col in data_from.collections:
            collections.append(col)

    return collections
