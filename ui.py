from typing import Text
import bpy

from . import functions

class VIEW3D_PT_rbr_tools(bpy.types.Panel):
    bl_label = "Jorski's RBR tools"
    bl_category = "RBR Tools"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"

    @classmethod
    def poll(cls, context):
        return context.mode in {'OBJECT'}

    def draw(self, context):
        rbr_props = context.scene.jorskis_rbr_properties

        layout = self.layout

        row = layout.row().label(text='RBR Split:')
        row = layout.row()
        box = row.box()
        row = box.row()
        row = row.label(text='Split range(m):')
        row = box.row()
        col = row.column().prop(rbr_props, 'rbr_split_range')
        col = row.column().operator('view3d.rbr_split_button', text='Split')

        row = layout.row().label(text='Create proxy:')
        row = layout.row()
        box = row.box()
        row = box.row()
        row = row.label(text='Proxy count:')
        row = box.row()
        col = row.column().prop(rbr_props, 'rbr_proxy_range')
        col = row.column().operator('view3d.rbr_proxy_button', text='Make proxy')

        row = layout.row().label(text='Transform tools:')

        row = layout.row()
        box = row.box()
        box.label(text='Only transform:')
        row = box.row()
        row = box.row()
        col = row.column().prop(rbr_props, 'rbr_transform_location')
        col = row.column().prop(rbr_props, 'rbr_transform_rotation')
        col = row.column().prop(rbr_props, 'rbr_transform_scale')
        row = box.row()
        col = row.column().operator('view3d.rbr_locrot_button', text='Transform objects')

        if not (rbr_props.rbr_transform_location or rbr_props.rbr_transform_rotation):
            row.enabled = False

        row = layout.row()
        box = row.box()
        box.label(text='Copy and transform:')
        row = box.row()
        col = row.column().operator('view3d.rbr_copy_and_locrot_button', text='Copy & transform')

class VIEW3D_PT_rbr_tools_editmode(bpy.types.Panel):
    bl_label = "Jorski's RBR tools"
    bl_category = "RBR Tools"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"

    @classmethod
    def poll(cls, context):
        return context.mode in {'EDIT_MESH'}

    def draw(self, context):
        rbr_props = context.scene.jorskis_rbr_properties

        layout = self.layout

        row = layout.row().label(text='Rotate UV faces:')
        row = layout.row()
        box = row.box()
        row = box.row()
        col = row.column().operator('view3d.rbr_uvrotate_button', text='Rotate UVs')

        row = layout.row().label(text='Pack UV faces:')
        row = layout.row()
        box = row.box()
        row = box.row()
        col = row.column().operator('view3d.rbr_uvmanipulate_button', text='Pack UVs')

class VIEW3D_PT_locrot_button(bpy.types.Operator):
    bl_idname = 'view3d.rbr_locrot_button'
    bl_label = 'Button'
    bl_description = 'Copy selected objects transformation to active object'

    def execute(self, context):

        return functions.copy_locrot(self, context)

class VIEW3D_PT_copy_and_locrot_button(bpy.types.Operator):
    bl_idname = 'view3d.rbr_copy_and_locrot_button'
    bl_label = 'Button'
    bl_description = 'Copy active object to selected objects locations & rotations'

    def execute(self, context):

        return functions.copy_and_locrot(self, context)

class VIEW3D_PT_split_button(bpy.types.Operator):
    bl_idname = 'view3d.rbr_split_button'
    bl_label = 'Button'
    bl_description = 'Split mesh'

    def execute(self, context):

        functions.rbr_split(self, context)

        return{'FINISHED'}

class VIEW3D_PT_uvrotate_button(bpy.types.Operator):
    bl_idname = 'view3d.rbr_uvrotate_button'
    bl_label = 'Button'
    bl_description = 'Rotate individual UV faces'

    def execute(self, context):

        functions.uv_rotate()

        return{'FINISHED'}

class VIEW3D_PT_uvmanipulate_button(bpy.types.Operator):
    bl_idname = 'view3d.rbr_uvmanipulate_button'
    bl_label = 'Button'
    bl_description = 'Pack individual UV faces near the actual texture'

    def execute(self, context):

        functions.uv_manipulation()

        return{'FINISHED'}

class VIEW3D_PT_proxy_button(bpy.types.Operator):
    bl_idname = 'view3d.rbr_proxy_button'
    bl_label = 'Button'
    bl_description = 'Create proxy object from linked library'

    def execute(self, context):
        scene = context.scene
        rbr_props = scene.jorskis_rbr_properties

        orig_object = context.view_layer.objects.active

        # If object is not linked return error
        if orig_object.library == None:
            self.report({'ERROR'}, '{} is not a linked library object'.format(orig_object.name))
            return {'CANCELLED'}

        proxys = []
        x = rbr_props.rbr_proxy_range
        i = 0
        while i < x:
            bpy.ops.object.proxy_make()
            proxys.append(context.view_layer.objects.active)
            context.view_layer.objects.active = orig_object
            i += 1

        for ob in proxys:
            ob.select_set(True)


        return{'FINISHED'}

class VIEW3D_PT_connect_button(bpy.types.Operator):
    bl_idname = 'scene.rbr_connect_button'
    bl_label = 'Button'

    def execute(self, context):

        functions.connect(self, context)

        return{'FINISHED'}

class VIEW3D_PT_import_button(bpy.types.Operator):
    bl_idname = 'scene.rbr_import_button'
    bl_label = 'Button'
    asset : bpy.props.StringProperty(default = '')

    def execute(self, context):
        scene = context.scene
        rbr_props = scene.jorskis_rbr_properties

        project_path = rbr_props.rbr_project_path
        folder = rbr_props.rbr_category
        file = self.asset

        # path to the blend
        filepath = '{0}\\{1}\\{2}'.format(project_path, folder, file)

        # name of object(s) to append or link
        obj_name = ''

        # append, set to true to keep the link to the original file
        link = rbr_props.rbr_link

        # link all objects starting with 'Cube'
        with bpy.data.libraries.load(filepath, link=link, relative=True) as (data_from, data_to):
            data_to.objects = [name for name in data_from.objects if name.startswith(obj_name)]

        #link object to current scene and inform if it's in already
        objects_in_scene = []

        for obj in data_to.objects:
            if obj is not None:
                if obj.name in context.scene.objects:
                    objects_in_scene.append(obj.name)
                else:
                    context.collection.objects.link(obj)
                    context.view_layer.objects.active = obj
                    if rbr_props.rbr_make_proxy and link:
                        bpy.ops.object.proxy_make()

        if objects_in_scene:
            self.report({'INFO'}, "Skipping objects: " + ", ".join(objects_in_scene) + ", which are already in scene")

        VIEW3D_PT_connect_button.execute(self, context)

        return{'FINISHED'}

class VIEW3D_PT_import_collection_button(bpy.types.Operator):
    bl_idname = 'scene.rbr_import_collection_button'
    bl_label = 'Button'
    asset : bpy.props.StringProperty(default = '')

    def execute(self, context):
        scene = context.scene
        rbr_props = scene.jorskis_rbr_properties

        import_location = scene.cursor.location

        project_path = rbr_props.rbr_project_path
        folder = rbr_props.rbr_category
        file = self.asset.split('-')[0]

        # path to the blend
        filepath = '{0}\\{1}\\{2}'.format(project_path, folder, file)

        # name of collection(s) to append or link
        col_name = self.asset.split('-')[1]

        # append, set to true to keep the link to the original file
        link = rbr_props.rbr_link

        # link all objects starting with 'Cube'
        with bpy.data.libraries.load(filepath, link=link, relative=True) as (data_from, data_to):
            data_to.collections = [name for name in data_from.collections if name == col_name]

        # link/append
        if link == False:
            for col in data_to.collections:
                if col is not None:
                    bpy.context.scene.collection.children.link(col)
        else:
            for col in data_to.collections:
                bpy.ops.object.collection_instance_add(collection=col.name, location=import_location)

        return{'FINISHED'}

class PANEL_PT_connect_menu(bpy.types.Panel):
    bl_label = "RBR Asset Loader"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "scene"

    def draw_object_menu(self, context):
        layout = self.layout
        scene = context.scene
        rbr_props = scene.jorskis_rbr_properties

        row = layout.row()
        row.prop(rbr_props, 'rbr_category')
        row.label(text='')
        row.prop(rbr_props, 'rbr_link')
        col = row.column()
        col.prop(rbr_props, 'rbr_make_proxy')
        if not rbr_props.rbr_link:
            col.enabled = False

        box = layout.box()
        split = box.split(align=True, factor=0.6)

        # Row 1
        row1 = split.row(align=True)
        col = row1.column(align=True)
        col.box().label(text='Asset:')

        for folder in functions.blend_dic:
            if folder == rbr_props.rbr_category:
                for asset in functions.blend_dic[folder]:
                    col.box().label(text=asset[0])

        # Row 2
        row2 = split.row(align=True)
        col = row2.column(align=True)
        col.box().label(text='Presence:')

        for folder in functions.blend_dic:
            if folder == rbr_props.rbr_category:
                for asset in functions.blend_dic[folder]:
                    col.box().label(text='{0:.1f} %'.format(asset[1]))

        # Row 3
        row3 = split.row(align=True)
        col = row3.column(align=True)
        col.box().label(text='')

        for folder in functions.blend_dic:
            if folder == rbr_props.rbr_category:
                for asset in functions.blend_dic[folder]:
                    if asset[1] == 100:
                        icon = 'CANCEL'
                        enable = False
                        depress = True
                    else:
                        icon = 'ADD'
                        enable = True
                        depress = False

                    col1 = col.column(align=True)
                    col1.box().operator('scene.rbr_import_button', text='Import', icon=icon, depress=depress).asset = asset[0]
                    col1.enabled = enable

    def draw_collection_menu(self, context):
        layout = self.layout
        scene = context.scene
        rbr_props = scene.jorskis_rbr_properties

        row = layout.row()
        row.prop(rbr_props, 'rbr_category')
        row.label(text='')
        row.prop(rbr_props, 'rbr_link')
        row.label(text='')

        box = layout.box()
        split = box.split(align=True)

        # Row 1
        row1 = split.row(align=True)
        col = row1.column(align=True)
        col.box().label(text='Asset')

        col = row1.column(align=True)
        col.box().label(text='Collections')

        col = row1.column(align=True)
        col.box().label(text='Import')


        # Rows

        for folder in functions.blend_dic:
            if folder == rbr_props.rbr_category:

                for asset in functions.blend_dic[folder]:
                    row = box.row(align=True)
                    col1 = row.column(align=True)
                    col1.label(text=asset[0])

                    if len(asset[1]) > 0:

                        collections = asset[1]

                        for i, collection in enumerate(collections):
                            col2 = row.column(align=True)
                            col2.label(text=collection)

                            col3 = row.column()
                            col3.operator('scene.rbr_import_collection_button', text='Import').asset = '{}-{}'.format(asset[0], collection)

                            if (i < len(collections)-1):
                                row = box.row(align=True)
                                row.label(text='')
                            else:
                                box.row(align=True).separator()
                    else:
                        collections = None
                        col2 = row.column(align=True)
                        col2.label(text="No collections available")
                        col3 = row.column()
                        col3.label(text='')
                        box.row(align=True).separator()


    def draw(self, context):
        layout = self.layout
        scene = context.scene
        rbr_props = scene.jorskis_rbr_properties

        row = layout.row()
        row.label(text='Project path:')
        row.label(text='')
        row = layout.row()
        row.prop(rbr_props, 'rbr_use_root', text='Use root path')
        row = layout.row()
        row.prop(rbr_props, 'rbr_project_path', text='')
        if rbr_props.rbr_use_root:
            row.enabled = False

        row = layout.row()
        row.label(text='Exclude folders:')
        row.label(text='')

        row = layout.row()
        row.prop(rbr_props, 'rbr_exclude', text='')
        row.prop(rbr_props, 'rbr_use_collection_import', text='Use collection import')

        row = layout.row()
        if rbr_props.rbr_connected:
            text = 'Refresh'
        else:
            text = 'Connect'
        row.operator('scene.rbr_connect_button', text=text)
        row.label(text='')

        row = layout.row()
        row.label(text='')

        if functions.blend_dic and not rbr_props.rbr_use_collection_import:
            self.draw_object_menu(context)
        else:
            self.draw_collection_menu(context)

class WMFileSelector(bpy.types.Operator):
    bl_idname = "scene.rbr_project_path_selector"
    bl_label = "Asset folder"

    filename_ext = ""

    def execute(self, context):
        fdir = self.properties.filepath
        context.scene.jorskis_rbr_properties.rbr_project_path = fdir
        return{'FINISHED'}
