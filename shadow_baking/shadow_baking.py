import bpy


# Transform baked shadows from file to vertex colors
def bake_uv_to_vc(image_name):
    # Lookup the image by name. Easier than trying to figure out which one is
    # currently active
    image = bpy.data.images[image_name]

    width = image.size[0]
    height = image.size[1]

    # delete existing default vertex color
    if 'Col' in bpy.context.object.data.vertex_colors:
        bpy.context.object.data.vertex_colors.remove(bpy.context.object.data.vertex_colors['Col'])

    # Create vertex color if missing
    if 'shadow_map' in bpy.context.object.data.vertex_colors:
        bpy.context.object.data.vertex_colors.remove(bpy.context.object.data.vertex_colors['shadow_map'])
        bpy.context.object.data.vertex_colors.new(name='shadow_map', do_init=True)
    else:
        bpy.context.object.data.vertex_colors.new(name='shadow_map', do_init=True)

    # Keep UVs in the within the bounds to avoid out-of-bounds errors
    def _clamp_uv(val):
        return max(0, min(val, 1))

    # Need to set the mode to VERTEX_PAINT, otherwise the vertex color data is
    # empty for some reason
    ob = bpy.context.object
    bpy.ops.object.mode_set(mode='VERTEX_PAINT')

    # Caching the image pixels makes this *much* faster
    local_pixels = list(image.pixels[:])

    for face in ob.data.polygons:
        for vert_idx, loop_idx in zip(face.vertices, face.loop_indices):
            uv_coords = ob.data.uv_layers.active.data[loop_idx].uv

            # Just sample the closest pixel to the UV coordinate. If you need
            # higher quality, an improved approach might be to implement
            # bilinear sampling here instead
            target = [round(_clamp_uv(uv_coords.x) * (width - 1)), round(_clamp_uv(uv_coords.y) * (height - 1))]
            index = ( target[1] * width + target[0] ) * 4

            bpy.context.object.data.vertex_colors["shadow_map"].data[loop_idx].color[0] = local_pixels[index]
            bpy.context.object.data.vertex_colors["shadow_map"].data[loop_idx].color[1] = local_pixels[index + 1]
            bpy.context.object.data.vertex_colors["shadow_map"].data[loop_idx].color[2] = local_pixels[index + 2]
            bpy.context.object.data.vertex_colors["shadow_map"].data[loop_idx].color[3] = local_pixels[index + 3]

    bpy.ops.object.mode_set(mode='OBJECT')

# Create UV Map and unwrap it as lightmap pack
def uv_lightmap():
    context = bpy.context
    object = context.object
    uv_layers = object.data.uv_layers

    if 'shadow_map' in uv_layers:
        shadow_map = uv_layers['shadow_map']
        uv_layers.remove(shadow_map)

    bpy.ops.mesh.uv_texture_add()
    uv_layers[len(uv_layers) - 1].name = 'shadow_map'

    # Alle 0.1 PREF_MARGIN_DIV arvot aiheuttavat varjojen sotkeutumista
    bpy.ops.uv.lightmap_pack(PREF_CONTEXT='ALL_FACES', PREF_MARGIN_DIV=0.1)
    uv_layers["shadow_map"].active_render = True

    # ?? IDEA ??
    # objects = []
    #
    # for object in bpy.data.objects:
    #     if object.select_get() == True:
    #         objects.append(object)
    #         object.select_set(False)
    #
    #
    # for object in objects:
    #     object.select_set(True)
    #     bpy.ops.uv.lightmap_pack(PREF_CONTEXT='ALL_FACES', PREF_PACK_IN_ONE=False, PREF_NEW_UVLAYER=True, PREF_MARGIN_DIV=0.1)
    #     object.select_set(False)


def create_shadowmap(map_size):
    images = bpy.data.images

    if 'shadow_map' in images:
        shadow_map = images['shadow_map']
        images.remove(shadow_map)

    bpy.data.images.new('shadow_map', map_size, map_size)

# Create material for baking and set the shadow map image active for baking
def create_material():

    def add_node(type, x, y):
        new_node = nodes.new(type)
        new_node.location.x = x
        new_node.location.y = y

        return new_node

    context = bpy.context
    object = context.object
    materials = bpy.data.materials
    material_slots = object.material_slots

    # Remove previous material
    if 'shadow_map' in materials:
        shadow_map = materials['shadow_map']
        materials.remove(shadow_map)

    shadow_map = materials.new('shadow_map')

    object.active_material_index = 0
    original_material = material_slots[0].material
    object.active_material = shadow_map

    material = materials['shadow_map']

    material.use_nodes = True
    node_tree = material.node_tree
    nodes = node_tree.nodes
    links = node_tree.links

    diff = add_node('ShaderNodeTexImage', -350, 350)
    diff.image = bpy.data.images['shadow_map']

    nodes.active = diff

    return original_material

def select_partially_colored_faces():
    ob = bpy.context.active_object

    #vertex colors are in fact stored per loop-vertex -&gt; MeshLoopColorLayer
    if ob.type == 'MESH':

        #how many loops do we have ?
        loops = len(ob.data.loops)
        verts = len(ob.data.vertices)
        vertex_cols = {}
        vertex_no_col_list = []

        #go through each vertex color layer
        for vcol in ob.data.vertex_colors:
            # look into each loop's vertex ? (need to filter out double entries)
            visit = verts * [False]
            colors = {}

            for l in range(loops):
                v = ob.data.loops[l].vertex_index
                c = vcol.data[l].color
                if not visit[v]:
                    colors[v] = c
                    visit[v] = True

            sorted(colors)

            for v, c in colors.items():

                vertex_cols[v] = "{0}".format((c[0], c[1], c[2]))

        partially_colored_faces = []

        for face in ob.data.polygons:
            # colored_list = set(vertex_col_list).intersection(list(face.vertices))
            # noncolored_list = set(vertex_no_col_list).intersection(list(face.vertices))
            face_vertices = set(list(face.vertices))
            colors = []
            for vertex in face_vertices:

                colors.append(vertex_cols[vertex])

            # print(colors)
            if len(set(colors)) > 1:
                partially_colored_faces.append(face)

            # for vertice in face_vertices:
            #     print('Vertex: ', vertice)

            # if face_vertices == colored_list or face_vertices == noncolored_list:
            #     pass
            # else:
            #     partially_colored_faces.append(face)

        # print('')

        for face in ob.data.polygons:
            if face in partially_colored_faces:
<<<<<<< HEAD

                # set the min face size to select
=======
>>>>>>> c9152c286f61b43a5cda7c9f307fe596fd94f4ed
                if face.area > 0.75:
                    face.select = True
                else:
                    face.select = False
            else:
                face.select = False

def smooth_vertex_colors():
    objects = bpy.context.selected_objects

    for object in objects:
        bpy.context.view_layer.objects.active = object

        bpy.ops.paint.vertex_paint_toggle()
        bpy.context.object.data.use_paint_mask_vertex = True
        bpy.ops.paint.vert_select_all(action='SELECT')
        bpy.ops.paint.vertex_color_smooth()
        bpy.ops.paint.vertex_paint_toggle()

map_size = 2048

objects = bpy.context.selected_objects

for object in objects:
    object.select_set(False)

for object in objects:

    if (len(object.data.polygons) < 1):
<<<<<<< HEAD
        print('skipping object {}...'.format(object.name))
=======
        print('{} not a MESH object, skipping...'.format(object.name))
>>>>>>> c9152c286f61b43a5cda7c9f307fe596fd94f4ed
        continue;

    object.select_set(True)
    bpy.context.view_layer.objects.active = object

    print ('Create shadowmap:')
    create_shadowmap(map_size)

    print ('Create UV Lightmap:')
    uv_lightmap()

    print ('Create material:')
    original_material = create_material()

    # Bake shadows to shadow map image
    bpy.context.scene.cycles.samples = 2
    print ('Bake:')
    bpy.ops.object.bake(type='SHADOW', use_clear=True)

    bpy.context.object.data.uv_layers[0].active_render = True
    bpy.context.object.active_material = original_material

    print ('Bake uv to vc:')
    bake_uv_to_vc('shadow_map')

    print ('Get vertex color info:')
    select_partially_colored_faces()

    print ('\nObject done: {}\n'.format(object.name))
    object.select_set(False)

for object in objects:
    uv_layers = object.data.uv_layers
    shadow_map = uv_layers['shadow_map']
    uv_layers.remove(shadow_map)
    object.select_set(True)
