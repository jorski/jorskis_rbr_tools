# Python Export Collision Mesh script

import bpy
import bmesh
import struct

type = '.cms'
path = 'C:\\Users\\joona\\Blender\\scripts\\rbr\\'
ob_name = bpy.context.object.name
bin_name = ob_name + type

obj = bpy.context.object

# Get the active mesh
me = bpy.context.object.data


# Get a BMesh representation
bm = bmesh.new()   # create an empty BMesh
bm.from_mesh(me)   # fill it in from a Mesh


# Modify the BMesh, can do anything here...
n_faces = 0
n_verts = 0

for f in me.polygons:
    n_faces += 1

for v in me.vertices:
    n_verts += 1

print('Name: {}'.format(ob_name))
print('Faces: {}'.format(n_faces))
print('Vertexes: {}'.format(n_verts))
#n_vert = []

# Write file
with open(path + bin_name, 'wb') as file:
    file.write(ob_name.encode())

    file.write(struct.pack('B', 0))
    file.write(struct.pack('l', 0))
    file.write(struct.pack('B', 0))
    file.write(struct.pack('B', 0))
    file.write(struct.pack('l', 0))

    file.write(struct.pack('l', n_verts))

    print('start export...')

    for i, v in enumerate(bm.verts):
        print('{} {} {}'.format(v.co.x, v.co.y, v.co.z))

        data = struct.pack('f',v.co.x)
        file.write(data)
        data = struct.pack('f',v.co.y)
        file.write(data)
        data = struct.pack('f',v.co.z)
        file.write(data)

    print(n_faces)
    file.write(struct.pack('l', n_faces))

    for f in me.polygons:
        face1 = f.vertices[0]
        face2 = f.vertices[1]
        face3 = f.vertices[2]

        print('{} {} {}'.format(face2, face3, face1))
        file.write(struct.pack('B', 0))

        file.write(struct.pack('l', face2))
        file.write(struct.pack('l', face3))
        file.write(struct.pack('l', face1))

    file.write(struct.pack('l', 0))

    # close the file
    file.close()

# free and prevent further access
bm.free()
